var html2json = require("html2json");

var http = require("http");
var querystring = require("querystring");
var urlParser = require("url");

var server = http.createServer(function(req, res){
    var urlParts = urlParser.parse(req.url);
    var query = querystring.parse(urlParts.query);
    res.setHeader("Content-Type", "text/json; charset=utf-8");
    var data = '';
    req.on("data", function(chunk) {
        data += chunk.toString();
    });
    req.on("end", function(){
        var formData = querystring.parse(data);
        var pass = '';
        if (formData.url) {
            pass = formData.url;
        } else {
            pass = formData.html;
        }
        html2json.html2json(pass, {pretty: !!formData.pretty, trim: !!formData.trim}, function(err, result){
            if (err) {
                console.log(err);
                res.end(err.toString());
                return;
            }
           
            res.write(result);
            res.end();
        });
    });
});


server.listen(8080);
